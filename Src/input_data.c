/* ///////////////////////////////////////////////////////////////////// */
/*! 
  \file  
  \brief Provide basic functionality for reading input data files.

  Collects a number of functions for opening, reading and interpolating
  initial conditions from user-supplied binary data file(s).
  The geometry and dimensions of the input grid can be different from 
  the actual grid employed by PLUTO, as long as the coordinate geometry
  transformation has been implemented.
  The input grid and data files should employ the same format and 
  conventions employed by PLUTO.
  Specifically:
  
  - Gridfile: coordinates should be written using the standard
              PLUTO 4 grid format.
  - Datafile: variables should be written in single or multiple
              binary data file using eighter single or double precision.
              The file extension must be ".flt" or ".dbl" for the former and
              the latter, respectively.
     
  InputDataOpen() initializes the inputData structure for a given
  input field (e.g. density) iby reading grid size and coordinates,
  geometry, precision, endianity, etc..

  InputDataInterpolate() is used to interpolate the given field from the
  input coordinates to the desired coordinate location using bi- or
  tri-linear interpolation to fill the data array.

  The input data is stored in a buffer by reading ::ID_NZ_MAX planes at
  a time to save computational memory.
  The value of ::ID_NZ_MAX can be changed from your personal \c definitions.h.
  This task is performed in the InputDataReadSlice() function.

  \authors A. Mignone (mignone@ph.unito.it)\n
  \date    Nov 17, 2016
*/
/* ///////////////////////////////////////////////////////////////////// */
#include"pluto.h"

#define ID_NVAR_MAX 64
#ifndef ID_NZ_MAX
 #define ID_NZ_MAX   4  /**< Maximum size (in the 3rd dimension) of the
                             input buffer. */
#endif

/* How many cells (in each coordinate) to use either side of the center */
#ifndef ID_CENT_CELLS
 #define ID_CENT_CELLS 5
#endif

typedef struct inputData_{
  char fname[300];
  size_t dsize;
  int nx1, nx2, nx3;
  int il, jl, kl;
  int ih, jh, kh;
  int lnx1, lnx2, lnx3;
  int cil, cjl, ckl;
  int cih, cjh, ckh;
  int cnx1, cnx2, cnx3;
  int geom;
  int swap_endian;
  int klast;        /**< The index of the last read plane */
  double *x1;
  double *x2;
  double *x3;
  double ***Vin;    /**< Input buffer array (containing at most ::ID_NZ_MAX
                         planes at a time */
  double ***Vin_central_region;
  int64_t offset;
} inputData;

static inputData id_stack[ID_NVAR_MAX];

/* ********************************************************************* */
int InputDataOpen(char *data_fname, char *grid_fname, char *endianity, int pos)
/*!
 * Initialize access to input data file by assigning values to 
 * grid-related information (geometry, number of points, etc...).
 * This function should be called just once for input-data initialization.
 *
 * \param [in] data_fname    the name of the input binary data file 
 * \param [in] grid_fname    the name of the corresponding grid file
 * \param [in] endianity     an input string ("little" or "big") giving
 *                           the byte-order of how the input data file
 *                           was originally written.
 *                           If an empty string is supplied, no change is
 *                           made.
 * \param [in] pos           an integer specifying the position of
 *                           the variable in the file.
 *
 *********************************************************************** */
{
  char *sub_str, sline[512];
  const char delimiters[] = " \t\r\f\n";
  int    i, ip, success;
  int    indx; /* The 1st available element in the id_stack structure */
  size_t str_length;
  double xl, xr;
  double x1l, x2l, x3l;
  double x1h, x2h, x3h;
  double cx1, cx2, cx3;
  int il, jl, kl;
  int ih, jh, kh;
  inputData *id;
  fpos_t file_pos;
  FILE *fp;

  print ("> Input data:\n\n");

/* --------------------------------------------------------------------- */
/*! 0. Find the 1st available (NULL pointer) element in the stack        */
/* --------------------------------------------------------------------- */

  indx = 0;
  while (id_stack[indx].Vin != NULL){
    indx++;
    if (indx == ID_NVAR_MAX){
      print ("! InputDataOpen(): max number of variables exceeded\n");
      print ("!                  indx = %d\n",indx);
      QUIT_PLUTO(1);
    }
  }

  print ("  Allocating memory for struct # %d\n",indx);
  id = id_stack + indx;

/* --------------------------------------------------------------------- */
/*! 1. Scan grid data file and try to determine the grid geometry 
       (\c id->geom). Search for tag "GEOMETRY:" and read the word that
       follows.                                                          */
/* --------------------------------------------------------------------- */

  fp = fopen(grid_fname,"r");
  if (fp == NULL){
    print ("! InputDataOpen(): grid file %s not found\n",grid_fname);
    QUIT_PLUTO(1);
  }
  success = 0;
  while(!success){
    fgets(sline,512,fp);
    sub_str = strtok(sline, delimiters);
    while (sub_str != NULL){
      if (!strcmp(sub_str,"GEOMETRY:")) {
        sub_str = strtok(NULL, delimiters);     
        success = 1;
        break;
      }
      sub_str = strtok(NULL, delimiters);     
    }  
  }
  
  if      (!strcmp(sub_str,"CARTESIAN"))   id->geom = CARTESIAN;
  else if (!strcmp(sub_str,"CYLINDRICAL")) id->geom = CYLINDRICAL;
  else if (!strcmp(sub_str,"POLAR"))       id->geom = POLAR;
  else if (!strcmp(sub_str,"SPHERICAL"))   id->geom = SPHERICAL;
  else{
    print ("! InputDataOpen(): unknown geometry\n");
    QUIT_PLUTO(1);
  }
    
  print ("  Input grid file:       %s\n", grid_fname);
  print ("  Input grid geometry:   %s\n", sub_str);

/* --------------------------------------------------------------------- */
/*! 2. Move file pointer to the first line of grid.out that does not
       begin with a "\c #".                                              */
/* --------------------------------------------------------------------- */

  success = 0;
  while(!success){
    fgetpos(fp, &file_pos);
    fgets(sline,512,fp);
    if (sline[0] != '#') success = 1;
  }

  fsetpos(fp, &file_pos);
  
/* --------------------------------------------------------------------- */
/*! 3. Read number of points, allocate grid arrays and store input
       grid coordinates into structure members \c id->nx1, \c id->x1,
       etc...                                                            */
/* --------------------------------------------------------------------- */
   
  fscanf (fp,"%d \n",&(id->nx1));
  id->x1 = ARRAY_1D(id->nx1, double);
  for (i = 0; i < id->nx1; i++){
    fscanf(fp,"%d  %lf %lf\n", &ip, &xl, &xr);
    id->x1[i] = 0.5*(xl + xr);
  }

  fscanf (fp,"%d \n",&(id->nx2));
  id->x2 = ARRAY_1D(id->nx2, double);
  for (i = 0; i < id->nx2; i++){
    fscanf(fp,"%d  %lf %lf\n", &ip, &xl, &xr);
    id->x2[i] = 0.5*(xl + xr);
  }

  fscanf (fp,"%d \n",&(id->nx3));
  id->x3 = ARRAY_1D(id->nx3, double);
  for (i = 0; i < id->nx3; i++){
    fscanf(fp,"%d  %lf %lf\n", &ip, &xl, &xr);
    id->x3[i] = 0.5*(xl + xr);
  }
  fclose(fp);

/* -- reset grid with 1 point -- */

  if (id->nx1 == 1) id->x1[0] = 0.0;
  if (id->nx2 == 1) id->x2[0] = 0.0;
  if (id->nx3 == 1) id->x3[0] = 0.0;

  print ("  Input grid extension:  x1 = [%12.3e, %12.3e] (%d points)\n",
             id->x1[0], id->x1[id->nx1-1], id->nx1);
  print ("\t\t\t x2 = [%12.3e, %12.3e] (%d points)\n",
             id->x2[0], id->x2[id->nx2-1], id->nx2);
  print ("\t\t\t x3 = [%12.3e, %12.3e] (%d points)\n",
             id->x3[0], id->x3[id->nx3-1], id->nx3);
/* ---------------------------------------------------------- */
/*! 3.5. Calculate our coordinate limits for the data file.   */
/* ---------------------------------------------------------- */
  /* lower limits are set to our first ghost zones */
  x1l = g_localGhostDomBeg[IDIR];
  x2l = g_localGhostDomBeg[JDIR];
  x3l = g_localGhostDomBeg[KDIR];

  /* upper limits are set to our last ghost zones */
  x1h = g_localGhostDomEnd[IDIR];
  x2h = g_localGhostDomEnd[JDIR];
  x3h = g_localGhostDomEnd[KDIR];

  /* We convert our lower and upper limits from PLUTO coordinates to input grid coordinates */
  InputDataTransformGridCoordinates(indx, &x1l, &x2l, &x3l);
  InputDataTransformGridCoordinates(indx, &x1h, &x2h, &x3h);

  /* Find corresponding indicies for our coordinate limits.
   * Make sure to handle grids with only 1 point correctly */
  if (id->nx1 != 1) { 
      il = InputDataBinarySearch(id->nx1 - 1, x1l, id->x1);
      ih = InputDataBinarySearch(id->nx1 - 1, x1h, id->x1) + 1;
  } else {
      il = ih = 0;
  }

  if (id->nx2 != 1) { 
      jl = InputDataBinarySearch(id->nx2 - 1, x2l, id->x2);
      jh = InputDataBinarySearch(id->nx2 - 1, x2h, id->x2) + 1;
  } else {
      jl = jh = 0;
  }

  if (id->nx3 != 1) { 
      kl = InputDataBinarySearch(id->nx3 - 1, x3l, id->x3);
      kh = InputDataBinarySearch(id->nx3 - 1, x3h, id->x3) + 1;
  } else {
      kl = kh = 0;
  }

  /* Set our local lower limit indicies on the input data structure */
  id->il = il;
  id->jl = jl;
  id->kl = kl;

  /* Set our local upper limit indicies on the input data structure */
  id->ih = ih;
  id->jh = jh;
  id->kh = kh;

  /* Set the number of local points in each coordinate direction */
  id->lnx1 = ih - il + 1;
  id->lnx2 = jh - jl + 1;
  id->lnx3 = kh - kl + 1;

  /* Print some debugging information */
  print("\tLocal domain limits are (%4f->%4f, %4f->%4f, %4f->%4f)\n", x1l, x1h, x2l, x2h, x3l, x3h);
  print("\tInput data indicies are (%i->%i, %i->%i, %i->%i)\n", il, ih, jl, jh, kl, kh);
  print("\tInput data local count is (%i, %i, %i)\n", id->lnx1, id->lnx2, id->lnx3);
  print("\n");

/* ---------------------------------------------------------- */
/*! 3.6. Calculate our coordinate limits for the central region.   */
/* ---------------------------------------------------------- */
  /* our central coordinates are simply 0,0,0 (for CARTESIAN)
   * TODO: Will need to fix this to account for other geometries */
  cx1 = cx2 = cx3 = 0.0;

  /* Let's transform this from PLUTO coordinates into input grid coordinates */
  InputDataTransformGridCoordinates(indx, &cx1, &cx2, &cx3);

  /* Take our central coordinates and find the corresponding lower index in our input grid coordinate arrays */
  if (id->nx1 != 1) { 
      il = InputDataBinarySearch(id->nx1 - 1, cx1, id->x1);
  } else {
      il = 0;
  }

  if (id->nx2 != 1) { 
      jl = InputDataBinarySearch(id->nx2 - 1, cx2, id->x2);
  } else {
      jl = 0;
  }

  if (id->nx3 != 1) { 
      kl = InputDataBinarySearch(id->nx3 - 1, cx3, id->x3);
  } else {
      kl = 0;
  }

  /* Now we can set our lower and upper limits for the central region, using ID_CENT_CELLS */
  ih = il + ID_CENT_CELLS;
  jh = jl + ID_CENT_CELLS;
  kh = kl + ID_CENT_CELLS;

  il = il - ID_CENT_CELLS;
  jl = jl - ID_CENT_CELLS;
  kl = kl - ID_CENT_CELLS;

  /* Clamp our indicies to make sure that they don't go out of bounds */
  ih = MAX(0, MIN(ih, id->nx1-1));
  jh = MAX(0, MIN(jh, id->nx2-1));
  kh = MAX(0, MIN(kh, id->nx3-1));

  il = MAX(0, MIN(il, id->nx1-1));
  jl = MAX(0, MIN(jl, id->nx2-1));
  kl = MAX(0, MIN(kl, id->nx3-1));

  /* Now we set our central lower and upper limit indicies on the input data structure */
  id->cil = il;
  id->cjl = jl;
  id->ckl = kl;

  id->cih = ih;
  id->cjh = jh;
  id->ckh = kh;

  /* Set the number of central region points in each coordinate direction */
  id->cnx1 = ih - il + 1;
  id->cnx2 = jh - jl + 1;
  id->cnx3 = kh - kl + 1;

  /* Print some debugging information */
  print("\tCentral domain limits are (%4f->%4f, %4f->%4f, %4f->%4f)\n", id->x1[il], id->x1[ih], id->x2[jl], id->x2[jh], id->x3[kl], id->x3[kh]);
  print("\tInput data indicies are (%i->%i, %i->%i, %i->%i)\n", il, ih, jl, jh, kl, kh);
  print("\tInput data local count is (%i, %i, %i)\n", id->cnx1, id->cnx2, id->cnx3);
  print("\n");
  
/* ---------------------------------------------------------- */
/*! 4. Check if binary data file exists and allocate memory
       buffer \c id->Vin                                      */
/* ---------------------------------------------------------- */

  char   ext[] = "   ";

  sprintf (id->fname,"%s",data_fname);
  fp = fopen(id->fname, "rb");
  if (fp == NULL){
    print ("! InputDataOpen(): file %s does not exist\n", data_fname);
    QUIT_PLUTO(1);
  }
  fclose(fp);

  /* Allocate memory for local variable array */
  id->Vin = ARRAY_3D(id->lnx3, id->lnx2, id->lnx1, double);

  /* Allocate memory for central variable array */
  id->Vin_central_region = ARRAY_3D(id->cnx3, id->cnx2, id->cnx1, double);

/* ---------------------------------------------------------- */
/*! 5. Set endianity (\c id->swap_endian)                     */
/* ---------------------------------------------------------- */
 
  if ( (!strcmp(endianity,"big")    &&  IsLittleEndian()) ||
       (!strcmp(endianity,"little") && !IsLittleEndian())) {
    id->swap_endian = YES;
  }else{
    id->swap_endian = NO;
  }
  
  print ("  Input data file:       %s (endianity: %s) \n", 
           data_fname, endianity);
  
/* ---------------------------------------------------------- */
/*! 6. Set data size (\c id->dsize) by looking at the file
       extension (\c dbl or \c flt).                          */
/* ---------------------------------------------------------- */

  str_length = strlen(data_fname);
  for (i = 0; i < 3; i++) ext[i] = data_fname[str_length-3+i];

  if (!strcmp(ext,"dbl")){
    print ("  Precision:             (double)\n");
    id->dsize = sizeof(double);
  } else if (!strcmp(ext,"flt")) {
    print ("  Precision:\t\t single\n");
    id->dsize = sizeof(float);
  } else {
    print ("! InputDataRead: unsupported data type '%s'\n",ext);
    QUIT_PLUTO(1);
  }

/* ---------------------------------------------------------- */
/*! 7. Compute offset (\c id->offset in bytes) and
       initialize \c id->klast (last read plane) to -1        */
/* ---------------------------------------------------------- */

  id->offset = (int64_t)pos*id->dsize*id->nx1*id->nx2*id->nx3;
  id->klast  = -1;

  print ("  offset = %ld\n",id->offset);
  print ("\n");

  return indx;  /* The index of the id_stack[] array */
}


/* ********************************************************************* */
void InputDataClose(int indx)
/*!
 * Free memory and reset structure.
 *********************************************************************** */
{
  inputData *id = id_stack + indx;
  FreeArray1D((void *)id->x1);
  FreeArray1D((void *)id->x2);
  FreeArray1D((void *)id->x3);
  FreeArray3D((void *)id->Vin);
  id->Vin = NULL;
}
/* ********************************************************************* */
double InputDataInterpolate (int indx, double x1, double x2, double x3)
/*!
 * Perform bi- or tri-linear interpolation on external
 * dataset to compute vs[] at the given point {x1,x2,x3}.
 *
 * \param [in] indx   input data array element (handle)
 * \param [in] x1     coordinate point at which at interpolates are desired
 * \param [in] x2     coordinate point at which at interpolates are desired
 * \param [in] x3     coordinate point at which at interpolates are desired
 *
 * \return The interpolated value.
 *
 * The function performs the following tasks. 
 *********************************************************************** */
{
  int il = 0, jl = 0, kl = 0;
  int in_local_x1 = 1, in_local_x2 = 1, in_local_x3 = 1;
  int in_central_x1 = 1, in_central_x2 = 1, in_central_x3 = 1;
  int in_local = 0, in_central = 0;
  int ih, jh, kh;
  int im, jm, km;
  int i, j, k, nv;
  float  uflt;
  double udbl;
  double xx, yy, zz, v;
  double **Vlo, **Vhi;
  inputData *id = id_stack + indx;
  static FILE *fp;

/* --------------------------------------------------------------------- */
/*! - Convert PLUTO coordinates to input grid geometry if necessary.     */
/* --------------------------------------------------------------------- */

  InputDataTransformGridCoordinates(indx, &x1, &x2, &x3);

/* --------------------------------------------------------------------- */
/*! - Make sure point (x1,x2,x3) does not fall outside input grid range. 
      Limit to input grid edge otherwise.                                */
/* --------------------------------------------------------------------- */
   
  D_EXPAND(if      (x1 < id->x1[0])         x1 = id->x1[0];
           else if (x1 > id->x1[id->nx1-1]) x1 = id->x1[id->nx1-1];  ,
           
           if      (x2 < id->x2[0])         x2 = id->x2[0];
           else if (x2 > id->x2[id->nx2-1]) x2 = id->x2[id->nx2-1];  ,
           
           if      (x3 < id->x3[0])         x3 = id->x3[0];
           else if (x3 > id->x3[id->nx3-1]) x3 = id->x3[id->nx3-1]; )

/* --------------------------------------------------------------------- */
/*! - Use table lookup by binary search to  find the indices 
      il, jl and kl such that grid points of PLUTO fall between 
      [il, il+1], [jl, jl+1], [kl, kl+1].                                */
/* --------------------------------------------------------------------- */

  il = 0;
  ih = id->nx1 - 1;
  while (il != (ih-1)){
    im = (il+ih)/2;
    if   (x1 <= id->x1[im]) ih = im;   
    else                    il = im;
  }
  
  if (id->nx2 > 1){
    jl = 0;
    jh = id->nx2 - 1;
    while (jl != (jh-1)){
      jm = (jl+jh)/2;
      if (x2 <= id->x2[jm]) jh = jm;   
      else                  jl = jm;
    }
  }

  if (id->nx3 > 1){
    kl = 0;
    kh = id->nx3 - 1;
    while (kl != (kh - 1)){
      km = (kl+kh)/2;
      if (x3 <= id->x3[km]) kh = km;   
      else                  kl = km;
    }
  }

/* --------------------------------------------------------------------- */
/*! - Define normalized coordinates between [0,1]:
      - x[il+1] < x1[i] < x[il+1] ==> 0 < xx < 1
      - y[jl+1] < x2[j] < y[jl+1] ==> 0 < yy < 1
      - z[kl+1] < x3[k] < z[kl+1] ==> 0 < zz < 1                            */
/* --------------------------------------------------------------------- */

  xx = yy = zz = 0.0; /* initialize normalized coordinates */

  if (id->nx1 > 1) xx = (x1 - id->x1[il])/(id->x1[il+1] - id->x1[il]);  
  if (id->nx2 > 1) yy = (x2 - id->x2[jl])/(id->x2[jl+1] - id->x2[jl]);  
  if (id->nx3 > 1) zz = (x3 - id->x3[kl])/(id->x3[kl+1] - id->x3[kl]);

/* --------------------------------------------------------------------- */
/*! - Read data from disk (1 or 2 planes)                                */
/* --------------------------------------------------------------------- */


/*
print ("@@ Interpolation required at %f %f %f\n",x1,x2,x3);
print ("@@ Closest input coord.      %f %f %f\n",
        id->x1[il], id->x2[jl], id->x3[kl]);
print ("@@ Coord indices:            %d %d %d\n",il,jl,kl);
print ("@@ id->klast                 %d\n",id->klast);

static long int ncall=0;

  if ( (kl >= id->klast + ID_NZ_MAX - 1) || (id->klast == -1) ){
print ("@@ kl = %d, klast = %d; ncall = %d\n",kl,id->klast,ncall);
    InputDataReadSlice(indx, kl);
ncall = 0;
  }
ncall++;
*/


  if (id->klast == -1) {
    InputDataReadSlice(indx, kl);
  }

/* --------------------------------------------------------------------- */
/*! - Perform bi- or tri-linear interpolation.                           */
/* --------------------------------------------------------------------- */

  /* print("Interpolation indicies (global): (il, jl, kl) = %i, %i, %i\n", il, jl, kl); */

  /* Default Vlo array if only 1 point in 3rd dimension */
  Vlo = id->Vin[0];

  /* Check that our interpolation indicies lie in one of our ranges (local or central).
   * Note that we check each region individually, since we want to flag central everything
   * if we have already flagged local! */
  if (id->nx1 > 1) {
      in_local_x1 = in_central_x1 = 0;

      if (il >= id->il && il < id->ih) {
          in_local_x1 = 1;
      }
      if (il >= id->cil && il < id->cih) {
          in_central_x1 = 1;
      }
  }

  if (id->nx2 > 1) {
      in_local_x2 = in_central_x2 = 0;

      if (jl >= id->jl && jl < id->jh) {
          in_local_x2 = 1;
      }
      if (jl >= id->cjl && jl < id->cjh) {
          in_central_x2 = 1;
      }
  }

  if (id->nx3 > 1) {
      in_local_x3 = in_central_x3 = 0;

      if (kl >= id->kl && kl < id->kh) {
          in_local_x3 = 1;
      }
      if (kl >= id->ckl && kl < id->ckh) {
          in_central_x3 = 1;
      }
  }

  in_local = (in_local_x1 && in_local_x2 && in_local_x3);
  in_central = (in_central_x1 && in_central_x2 && in_central_x3);

  /* Set everything up correctly if we are COMPLETELY in central or local zones */
  if (in_central) {
      if (id->nx1 > 1) il -= id->cil;          
      if (id->nx2 > 1) jl -= id->cjl;          
      if (id->nx3 > 1) {
          kl -= id->ckl;          

          Vlo = id->Vin_central_region[kl];
          Vhi = id->Vin_central_region[kl+1];
      }
  } else if (in_local) {
      if (id->nx1 > 1) il -= id->il;
      if (id->nx2 > 1) jl -= id->jl;
      if (id->nx3 > 1) {
          kl -= id->kl;

          Vlo = id->Vin[kl];
          Vhi = id->Vin[kl+1];
      }
  } else {
      /* Otherwise throw a warning and return 0 */
      print("! InputDataInterpolate: Interpolation indices (%i, %i, %i) out of both local and central range\n", il, jl, kl);
      QUIT_PLUTO(1);
      /* v = 0.0; */
      /* return v; */
  }

  v =   Vlo[jl][il]*(1.0 - xx)*(1.0 - yy)*(1.0 - zz)  /* [0] is kl */
      + Vlo[jl][il+1]*xx*(1.0 - yy)*(1.0 - zz);
  if (id->nx2 > 1){
    v +=   Vlo[jl+1][il]*(1.0 - xx)*yy*(1.0 - zz)
         + Vlo[jl+1][il+1]*xx*yy*(1.0 - zz);
  }
  if (id->nx3 > 1){
    v +=   Vhi[jl][il]*(1.0 - xx)*(1.0 - yy)*zz
         + Vhi[jl][il+1]*xx*(1.0 - yy)*zz
         + Vhi[jl+1][il]*(1.0 - xx)*yy*zz
         + Vhi[jl+1][il+1]*xx*yy*zz;
  }

  return v;
}

/* ********************************************************************* */
void InputDataReadSlice(int indx, int kslice)
/*! 
 * Read ::ID_NZ_MAX slices from disk starting at the kslice vertical
 * index.
 *
 * \param [in] indx    the structure index (file handle)
 * \param [in] kslice  the starting slice
 *
 *********************************************************************** */
{
  int64_t offset;
  int64_t ibego, iendo, jbego, jendo, kbego, kendo;
  inputData *id = id_stack + indx;
  FILE *fp;

/* ----------------------------------------------------
   1. Compute offset.
      Here id->offset (in bytes) is the location of the
      variable in the file, while the second number is
      the vertical slice we want to read.
      Seek from beginning of the file.
   ---------------------------------------------------- */

  offset = id->offset;

  if ((fp = fopen(id->fname, "rb")) == NULL) {
      print("! InputDataInterpolate(): Error opening external environment file located at: %s\n", id->fname);
      QUIT_PLUTO(1);
  }
  fseek(fp, offset, SEEK_SET);

/* -----------------------------------------------------
   2. Read binary data at specified position.
   ----------------------------------------------------- */

  print("> Reading local region\n");

  /* Because we are not reading the entire variable, we need to compute our offsets into the variable section.
   * The file is structured so that i is varying fastest, k is varying slowest (i.e. in the form (k,j,i)).
   * This means that we need to take in to account our i size for j, and our i&j sizes for k.
   * {i,j,k}bego will contain the offset we need to seek to get to the specific patch we are reading in that coordinate,
   * while {i,j,k}endo will contain the offset we need to seek after reading, to get to the end of that coordinate */

  /* The following are in row-major order */
  ibego = id->il*id->dsize;
  jbego = id->jl*id->dsize*id->nx1;
  kbego = id->kl*id->dsize*id->nx1*id->nx2;

  iendo = (id->nx1 - id->lnx1 - id->il)*id->dsize;
  jendo = (id->nx2 - id->lnx2 - id->jl)*id->dsize*id->nx1;
  kendo = (id->nx3 - id->lnx3 - id->kl)*id->dsize*id->nx1*id->nx2;

  int nx[3] = {id->lnx1, id->lnx2, id->lnx3};
  int64_t beg[3] = {ibego, jbego, kbego};
  int64_t end[3] = {iendo, jendo, kendo};

  InputDataReadRegion(indx, id->Vin, fp, nx, beg, end);

  print("> Reading central region\n");
  
  /* Reset file offset */
  fseek(fp, offset, SEEK_SET);

  ibego = id->cil*id->dsize;
  jbego = id->cjl*id->dsize*id->nx1;
  kbego = id->ckl*id->dsize*id->nx1*id->nx2;

  iendo = (id->nx1 - id->cnx1 - id->cil)*id->dsize;
  jendo = (id->nx2 - id->cnx2 - id->cjl)*id->dsize*id->nx1;
  kendo = (id->nx3 - id->cnx3 - id->ckl)*id->dsize*id->nx1*id->nx2;

  int cnx[3] = {id->cnx1, id->cnx2, id->cnx3};
  int64_t cbeg[3] = {ibego, jbego, kbego};
  int64_t cend[3] = {iendo, jendo, kendo};

  InputDataReadRegion(indx, id->Vin_central_region, fp, cnx, cbeg, cend);

/* -- Update last successfully read slice -- */

  id->klast = id->lnx3 - 1;
  fclose(fp);
/*
print ("@@@ Offset = %d\n",offset);
for (k = 0; k < kmax   ; k++){
for (j = 0; j < id->nx2; j++){
for (i = 0; i < id->nx1; i++){
  print ("@@@ Input value (%d %d %d) = %f\n",i,j,k,id->Vin[k][j][i]);
}}}
*/
}

void InputDataReadRegion(int indx, double ***V_out, FILE *fp, int nx[3], int64_t beg[3], int64_t end[3]) {
/*! 
 * Read data region from disk, and save it to the array given by V_out
 *
 * \param [in] indx    the structure index (file handle)
 * \param [in] V_out   the data array to save the values too
 * \param [in] fp      the file handle for the data file
 * \param [in] nx      a 3-element vector containing the number of points in each dimension ([nx1, nx2, nx3])
 * \param [in] beg     the beginning indicies for this region in the data file ([ibeg, jbeg, kbeg])
 * \param [in] end     the end indicies for this region in the data file ([iend, jend, kend])
 *
 *********************************************************************** */
  int i,j,k, kmax;
  int err = 0;
  inputData *id = id_stack + indx;
  double udbl;
  float  uflt;

  int nx1 = nx[0];
  int nx2 = nx[1];
  int nx3 = nx[2];

  int64_t ibego = beg[0];
  int64_t jbego = beg[1];
  int64_t kbego = beg[2];

  int64_t iendo = end[0];
  int64_t jendo = end[1];
  int64_t kendo = end[2];
   
  if (id->dsize == sizeof(double)){

    /* seek to start of k */
    fseek(fp, kbego, SEEK_CUR);
    for (k = 0; k < nx3   ; k++) {
        /* seek to start of j */
        fseek(fp, jbego, SEEK_CUR);
        for (j = 0; j < nx2; j++) {
            /* seek to start of i */
            fseek(fp, ibego, SEEK_CUR);

            /* read values */
            for (i = 0; i < nx1; i++) {
              if (fread (&udbl, id->dsize, 1, fp) != 1){  
                  err += 1;
                  udbl = 0.0;
              }
              if (id->swap_endian) SWAP_VAR(udbl);
              V_out[k][j][i] = udbl;
            }

            /* seek to end of i */
            fseek(fp, iendo, SEEK_CUR);
        }
        /* seek to end of j */
        fseek(fp, jendo, SEEK_CUR);
    }
    /* seek to end of k */
    fseek(fp, kendo, SEEK_CUR);

   }else{

    /* seek to start of k */
    fseek(fp, kbego, SEEK_CUR);
    for (k = 0; k < nx3   ; k++) {
        /* seek to start of j */
        fseek(fp, jbego, SEEK_CUR);
        for (j = 0; j < nx2; j++) {
            /* seek to start of i */
            fseek(fp, ibego, SEEK_CUR);

            /* read values */
            for (i = 0; i < nx1; i++) {
              if (fread (&uflt, id->dsize, 1, fp) != 1){
                  err += 1;
                  uflt = 0.0;
              }
              if (id->swap_endian) SWAP_VAR(uflt);
              V_out[k][j][i] = (double)uflt;
            }

            /* seek to end of i */
            fseek(fp, iendo, SEEK_CUR);
        }
        /* seek to end of j */
        fseek(fp, jendo, SEEK_CUR);
    }
    /* seek to end of k */
    fseek(fp, kendo, SEEK_CUR);
  }

  if (err > 0) {
      print("! InputDataReadSlice(): Failed to read %i values. These have been set to 0.0\n", err);
  }
}

void InputDataTransformGridCoordinates(int indx, double* x1, double* x2, double* x3) {
  inputData *id = id_stack + indx;
#if GEOMETRY == CARTESIAN
  if (id->geom == GEOMETRY) {  

    /* same coordinate system: nothing to do */
     
  }else if (id->geom == CYLINDRICAL) {  
    double R, z, phi;
    R   = sqrt((*x1)*(*x1) + (*x2)*(*x2));
    phi = atan2(*x2,*x1);
    if (phi < 0.0) phi += 2.0*CONST_PI;
    z   = *x3;

    *x1 = R; *x2 = z; *x3 = phi;
  }else if (id->geom == POLAR) {  
    double R, phi, z;
    R   = sqrt((*x1)*(*x1) + (*x2)*(*x2));
    phi = atan2(*x2,*x1);
    if (phi < 0.0) phi += 2.0*CONST_PI;
    z   = *x3;

    *x1 = R; *x2 = phi; *x3 = z;
  }else if (id->geom == SPHERICAL){
    double r, theta, phi;
    r     = D_EXPAND((*x1)*(*x1), + (*x2)*(*x2), + (*x3)*(*x3));
    r     = sqrt(r);
    theta = acos((*x3)/r);
    phi   = atan2(*x2,*x1);
    if (phi   < 0.0) phi   += 2.0*CONST_PI;
    if (theta < 0.0) theta += 2.0*CONST_PI;
     
    *x1 = r; *x2 = theta; *x3 = phi;
  }else{
    print ("! InputDataTransformGridCoordinates(): invalid or unsupported coordinate transformation.\n");
    QUIT_PLUTO(1);
  }
#elif GEOMETRY == CYLINDRICAL
  if (id->geom == GEOMETRY) {  

    /* same coordinate system: nothing to do */
     
  }else if (id->geom == SPHERICAL) {  
    double r, theta, phi;
    r     = D_EXPAND((*x1)*(*x1), + (*x2)*(*x2), + 0.0);
    r     = sqrt(r);
    theta = acos(*x2/r);
    phi   = 0.0;
    if (theta < 0.0) theta += 2.0*CONST_PI;
     
    *x1 = r; *x2 = theta; *x3 = phi;
  }else{
    print ("! InputDataTransformGridCoordinates(): invalid or unsupported coordinate transformation.\n");
    QUIT_PLUTO(1);
  }
#elif GEOMETRY == POLAR
  if (id->geom == GEOMETRY) {  

    /* same coordinate system: nothing to do */
     
  }else if (id->geom == CARTESIAN) {  
    double x, y, z;
    x = (*x1)*cos(*x2);
    y = (*x1)*sin(*x2);
    z = *x3;
     
    *x1 = x; *x2 = y; *x3 = z;
  }else{
    print ("! InputDataTransformGridCoordinates(): invalid or unsupported coordinate transformation.\n");
    QUIT_PLUTO(1);
  }   
#elif GEOMETRY == SPHERICAL
  if (id->geom == GEOMETRY) {  

    /* same coordinate system: nothing to do */
     
  }else if (id->geom == CARTESIAN) {  
    double x, y, z;
    x = (*x1)*sin(*x2)*cos(*x3);
    y = (*x1)*sin(*x2)*sin(*x3);
    z = (*x1)*cos(*x2);
     
    *x1 = x; *x2 = y; *x3 = z;
  }else{
    print ("! InputDataTransformGridCoordinates(): invalid or unsupported coordinate transformation.\n");
    QUIT_PLUTO(1);
  }   
#endif
}

int InputDataBinarySearch(int h, double x, double *xarr) {
  int l, m;
  l = 0;
  while (l != (h-1)){
    m = (l+h)/2;
    if       (x <= xarr[m]) h = m;   
    else                    l = m;
  }
  return l;
}
